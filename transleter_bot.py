import os
import requests
import asyncio

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
from aiogram.contrib.fsm_storage.memory import MemoryStorage

from gtts import gTTS

from data.data import TOKEN, KEY, API_YAN_URL, MAX_SYMBOL_VOICE, MAX_SYMBOL_TEXT
# from data.data_product import TOKEN, KEY, API_YAN_URL, MAX_SYMBOL_VOICE, MAX_SYMBOL_TEXT
from data.keyboard import lang_inline_keyboard, answer_inline_keyboard, support_reply_keyboard, donat_inline_keyboard
from data.utils import OptionsStates, LangStates

loop = asyncio.get_event_loop()
bot = Bot(TOKEN, loop=loop)
dp = Dispatcher(bot, storage=MemoryStorage())

params = {'key': KEY, 'options': 1}


@dp.message_handler(commands=['start'], state='*')  # Responds to command /start
async def send_welcome(message: types.Message):

    await bot.send_message(message.chat.id,
                           "Hello, %s! I am a bot translator. "
                           "I can voice the translated text up to 300 characters. "
                           "Original language will be determined automatically. "
                           "To select translation language, use the /lang command. \n"
                           "To change select, use the /lang command again."
                           % (message.from_user.first_name,))


@dp.message_handler(commands=['lang'], state='*')  # Responds to command /lang
async def select_lang_to_translate(message: types.Message):
    keyboard = lang_inline_keyboard()

    await bot.send_message(message.chat.id,
                           text='Choose translation language:',
                           reply_markup=keyboard)


@dp.callback_query_handler(lambda c: c.data == '1' or c.data == '0', state='*')  # Voice confirmation
async def inline_select_lang_to_translate(callback_query: types.CallbackQuery):
    state = dp.current_state(user=callback_query.from_user.id)  # состояние пользователя

    if callback_query.data == '1':
        await state.update_data({'option': OptionsStates.all()[int(callback_query.data)]})  # обновление состояния
    else:
        await state.update_data({'option': OptionsStates.all()[0]})  # обновление состояния

    await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                message_id=callback_query.message.message_id,
                                text='Enter text to translate:')


@dp.callback_query_handler(lambda c: True, state='*')  # Completes the language selection
async def inline_select_lang_to_translate(callback_query: types.CallbackQuery):
    keyboard = answer_inline_keyboard()
    state = dp.current_state(user=callback_query.from_user.id)  # состояние пользователя
    await state.reset_data()  # сброс состояния
    lang_state = LangStates.all().index(callback_query.data)  # получаем отправленный код языка

    await state.set_data({'lang': LangStates.all()[int(lang_state)]})  # обновление состояния

    await bot.edit_message_text(chat_id=callback_query.message.chat.id,
                                message_id=callback_query.message.message_id,
                                text='Want to hear the pronunciation?',
                                reply_markup=keyboard)


@dp.message_handler(regexp='Support this project', state='*')  # Support this project
async def donation_message(message: types.Message):
    keyboard = donat_inline_keyboard()

    await bot.send_message(message.chat.id,
                           text='%s, you can make a donation for the development of this project. 💰'
                                'The funds will go to the development of the bot 🔝, '
                                'a new server for it and the creation of new applications 🆕. '
                                'Thank you for your participation in the development of the project.'
                                % (message.from_user.first_name,),
                           reply_markup=keyboard)


@dp.message_handler(content_types=['text'],
                    state=OptionsStates.all().extend(LangStates.all()))  # Translates and voices incoming text
async def translate(message: types.Message):
    keyboard = support_reply_keyboard()
    state = dp.current_state(user=message.from_user.id)
    data_dict = await state.get_data()  # получаем итоговое состояние
    # print(data_dict)  # оставим для отладки

    if len(message.text) <= MAX_SYMBOL_TEXT:  # проверяем длинну текста
        try:  # проверяем ключ params['lang']
            params['lang'] = data_dict['lang']
            params['text'] = message.text
            response = requests.post(API_YAN_URL, data=params, timeout=30).json()
            translated_text = ' '.join(response.get('text', []))

            if translated_text != message.text:  # на случай если оригинальный текст кривой
                await bot.send_message(message.chat.id, translated_text, reply_markup=keyboard)

                if data_dict['option'] == 'voice_true_state':  # если нужна озвучка, то

                    if len(translated_text) <= MAX_SYMBOL_VOICE:  # проверяем длинну текста для озвучки

                        try:  # пробуем создать папку для записи переводов
                            os.mkdir('users/%d' % (message.chat.id,))
                        except OSError:  # если папка уже есть, то pass
                            pass

                        try:  # пробуем озвучить и записать перевод
                            tts = gTTS(text=translated_text, lang=params['lang'])
                            tts.save('users/%d/%s.ogg' % (message.chat.id, params['lang']))

                            await bot.send_chat_action(message.chat.id, 'record_audio')
                            try:
                                voice = open('users/%d/%s.ogg' % (message.chat.id, params['lang']), 'rb')
                                await bot.send_voice(message.chat.id, voice, reply_markup=keyboard)
                                voice.close()
                            except FileNotFoundError:
                                # не понятно почему появляется ошибка у нового пользователя, при data_dict['option'] != 'voice_true_state'
                                pass
                        except ValueError:  # если ошибка в озвучке, то
                            await bot.send_message(message.chat.id,
                                                   text="Sorry, I can't voice this language",
                                                   reply_markup=keyboard)
                    else:  # если текст для озвучки слишком длинный, то
                        await bot.send_message(message.chat.id,
                                               text='Sorry, the text size exceeds 300 characters, '
                                                    'try reducing the text.')
                else:  # если озвучка не нужна, то
                    pass
            else:  # если ориг-ый текст кривой, то
                await bot.send_message(message.chat.id,
                                       text="Sorry, I couldn't determine the original language. Try again.")
        except KeyError:  # если params['lang'] нет, то:
            await bot.send_message(message.chat.id,
                                   text="Please, reboot command /lang",
                                   reply_markup=keyboard)
    else:  # если текст слишком длинный, то
        await bot.send_message(message.chat.id,
                               text='Sorry, this text is too long. Try reducing it.')


async def shutdown(dispatcher: Dispatcher):  # Закрываем хранилище
    await dispatcher.storage.close()
    await dispatcher.storage.wait_closed()


if __name__ == '__main__':
    executor.start_polling(dp, timeout=100, on_shutdown=shutdown)
