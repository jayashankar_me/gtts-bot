import os


async def clean_ogg():
    for root, dirs, files in os.walk("users"):
        for file in files:
            if file.endswith(".ogg"):
                os.remove(os.path.join(root, file))


async def clean_json():
    for root, dirs, files in os.walk("users"):
        for file in files:
            if file.endswith(".json"):
                os.remove(os.path.join(root, file))
