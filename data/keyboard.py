from aiogram import types
# from data.data import url_pp, url_qiwi
from data.data_product import QIWI_URL, PP_URL


def lang_inline_keyboard():
    keyboard = types.InlineKeyboardMarkup(row_width=3)
    button_1 = types.InlineKeyboardButton('Russian 🇷🇺', callback_data='ru')
    button_2 = types.InlineKeyboardButton('Беларускі 🇧🇾', callback_data='be')
    button_3 = types.InlineKeyboardButton('🇺🇦 Український', callback_data='uk')
    button_4 = types.InlineKeyboardButton('Polski 🇵🇱', callback_data='pl')
    button_5 = types.InlineKeyboardButton('English 🏴󠁧󠁢󠁥󠁮󠁧󠁿', callback_data='en')
    button_6 = types.InlineKeyboardButton('Deutsch 🇩🇪', callback_data='de')
    button_7 = types.InlineKeyboardButton('Le français 🇫🇷', callback_data='fr')
    button_8 = types.InlineKeyboardButton('Italiano 🇮🇹', callback_data='it')
    button_9 = types.InlineKeyboardButton('Español 🇪🇸', callback_data='es')
    button_10 = types.InlineKeyboardButton('Português 🇵🇹', callback_data='pt')
    button_11 = types.InlineKeyboardButton('Ελληνικά 🇬🇷', callback_data='el')
    button_12 = types.InlineKeyboardButton('Türk 🇹🇷', callback_data='tr')
    button_13 = types.InlineKeyboardButton('العربية 🇦🇪', callback_data='ar')
    button_14 = types.InlineKeyboardButton('فارسی 🇮🇷', callback_data='fa')
    button_15 = types.InlineKeyboardButton('יהודי 🇮🇱', callback_data='he')

    keyboard.add(button_1, button_2, button_3, button_4, button_5, button_6, button_7, button_8,
                 button_9, button_10, button_11, button_12, button_13, button_14, button_15)
    return keyboard


def answer_inline_keyboard():
    keyboard = types.InlineKeyboardMarkup()
    button_1 = types.InlineKeyboardButton('Yes', callback_data='1')
    button_2 = types.InlineKeyboardButton('No', callback_data='0')
    keyboard.add(button_1, button_2)
    return keyboard


def support_reply_keyboard():
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    button = types.KeyboardButton('Support this project')
    keyboard.add(button)
    return keyboard


def donat_inline_keyboard():
    keyboard = types.InlineKeyboardMarkup()
    button_1 = types.InlineKeyboardButton(text='PayPal 💵💶', url=PP_URL)
    button_2 = types.InlineKeyboardButton(text='Qiwi for rub.', url=QIWI_URL)
    keyboard.add(button_1, button_2)
    return keyboard
