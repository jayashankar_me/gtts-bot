import os

API_YAN_URL = 'https://translate.yandex.net/api/v1.5/tr.json/translate'

MAX_SYMBOL_TEXT = 600
MAX_SYMBOL_VOICE = 300

TOKEN = os.environ.get('TOKEN')
KEY = os.environ.get('KEY')
QIWI_URL = os.environ.get('QIWI_URL')
PP_URL = os.environ.get('PP_URL')
