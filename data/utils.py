from aiogram.utils.helper import Helper, HelperMode, ListItem


class OptionsStates(Helper):
    '''
    Класс состояний для выбора озвучки перевода
    '''
    mode = HelperMode.snake_case

    VOICE_FALSE_STATE = ListItem()
    VOICE_TRUE_STATE = ListItem()


class LangStates(Helper):
    '''
    Класс состояний для выбора языка перевода
    '''
    mode = HelperMode.snake_case

    RU = ListItem()
    BE = ListItem()
    UK = ListItem()
    PL = ListItem()
    EN = ListItem()
    DE = ListItem()
    FR = ListItem()
    IT = ListItem()
    ES = ListItem()
    PT = ListItem()
    EL = ListItem()
    TR = ListItem()
    AR = ListItem()
    FA = ListItem()
    HE = ListItem()
